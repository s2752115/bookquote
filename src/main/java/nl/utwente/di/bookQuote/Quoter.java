package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String s) {
        HashMap<String, Double> inputMap = new HashMap<>();
        inputMap.put("1", 10.0);
        inputMap.put("2", 45.0);
        inputMap.put("3", 20.0);
        inputMap.put("4", 35.0);
        inputMap.put("5", 50.0);
        return inputMap.getOrDefault(s,0.0) ;
    }
}
