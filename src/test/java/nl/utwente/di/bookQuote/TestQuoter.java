package nl.utwente.di.bookQuote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
public class TestQuoter {
    @Test
    public void testBook1() throws Exception {
        HashMap<String, Double> inputMap = new HashMap<>();
        inputMap.put("1", 10.0);
        inputMap.put("2", 45.0);
        inputMap.put("3", 20.0);
        inputMap.put("4", 35.0);
        inputMap.put("5", 50.0);
        inputMap.put("others", 0.0);

        Quoter quoter = new Quoter();

        double price = quoter.getBookPrice("1");
        Assertions.assertEquals(10.0, price, 0.0, "Price of book 1");



    }

}
